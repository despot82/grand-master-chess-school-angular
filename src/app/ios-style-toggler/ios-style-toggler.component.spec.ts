import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IosStyleTogglerComponent } from './ios-style-toggler.component';

describe('IosStyleTogglerComponent', () => {
  let component: IosStyleTogglerComponent;
  let fixture: ComponentFixture<IosStyleTogglerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IosStyleTogglerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IosStyleTogglerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
