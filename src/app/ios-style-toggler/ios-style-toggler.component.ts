import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-ios-style-toggler',
  templateUrl: './ios-style-toggler.component.html',
  styleUrls: ['./ios-style-toggler.component.css']
})
export class IosStyleTogglerComponent implements OnInit {

    @Input() checked: boolean | string;
    @Output() checkedChange = new EventEmitter<boolean>();

    constructor() {

        this.checked = false;
    }

    ngOnInit () {

    }

    onClicked() {

        this.checked = !this.checked;
        this.checkedChange.emit(this.checked);
    }

}
