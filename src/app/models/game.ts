export class Game {
    public id: number;
    public event: string;
    public site: string;
    public data: string;
    public round: string;
    public white: string;
    public black: string;
    public result: string;
    public annotator: string;
    public setUp: string;
    public fen: string;
    public plyCount: string;
    public eventDate: string;
    public pgn: string;
    public moves: string;
    public fenList: Array<string>;

    constructor () {
        // this.pgn2fen();
    }

    onInit () {

    }

    pgn2fen() {

        Init(this.fen);
        SetPgnMoveText(this.moves);

        let ff = '';
        let ff_new = '', ff_old;
        do {
            ff_old = ff_new;
            MoveForward(1);
            ff_new = GetFEN();

            if (ff_old !== ff_new) {
                ff += ff_new + '\n';
            }
            //alert(ff_new);
            //alert(ff_old);
        }
        while (ff_old !== ff_new);

        this.fenList = ff.split('\n');
        if (this.fenList[this.fenList.length - 1] === '' ) {
            this.fenList.pop();

        }
        console.log(this.fenList);

    }
}
