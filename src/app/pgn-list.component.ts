import { Component, OnInit } from '@angular/core';
import { Pgn } from './pgn';
import { Http } from '@angular/http';
import { Game } from './models/game';
import { Router} from '@angular/router';

@Component({
  selector: 'app-pgn-list',
  templateUrl: './pgn-list.component.html',
  styleUrls: ['./pgn-list.component.css']
})
export class PgnListComponent implements OnInit {
    games: Array<Game>;
    processedPgn;
    pgnHelper: Pgn;
    http: Http;
    router: Router;

    constructor(_pgnHelper: Pgn,
      _http: Http,
      _router: Router) {
        this.pgnHelper = _pgnHelper;
        this.http = _http;
        this.router = _router;
        this.games = new Array();

        this.pgnHelper.promiseToGetAllGamesFromPgn('school').then(
          (res) => {
              this.processedPgn = res;
              this.formatGames();
          },
          (res) => {
              console.log('promise failed');
          }
        );
    }

    ngOnInit() {
    }

    formatGames() {

        // Go two steps each, because game has metadata section and the actual game section (the one-liner)
        // So, each lop passage is one game actually
        for (let i = 0; i < this.processedPgn.length; i = i + 2) {
            const g: Game = new Game();

            g['id'] = i / 2;
            //console.log(g);
            this.games[i / 2] =  g;

            const metadataRawSplittedByNewline = this.processedPgn[i].split('\n');

            // Go through each piece of metadata
            for (let j = 0; j < metadataRawSplittedByNewline.length; j++) {

                const fieldName = metadataRawSplittedByNewline[j].split(' ')[0].substring(1).toLowerCase();

                let fieldValue = metadataRawSplittedByNewline[j].split('"')[1];

                if (fieldValue.substr(fieldValue.length - 1, 1) === ']' ) {
                    fieldValue = fieldValue.substr(0, fieldValue.length - 1);
                }

                if (fieldValue.substr(fieldValue.length - 1, 1) === '"' ) {
                    fieldValue = fieldValue.substr(0, fieldValue.length - 1);
                }

                g[fieldName] = fieldValue;

            }

            const secondDataPart = this.processedPgn[i + 1];
            g.moves = secondDataPart.split(']} ')[1];

            //alert(g.moves);
        }

    }

    gameItemClicked (gameId: number, gameEvent: string, gameFen: string, gameMoves: string) {
        //alert(gameId);
        //alert(gameEvent);
        //alert(gameFen);
        //alert(gameMoves);

        this.router.navigate(['game', gameId, gameEvent, gameFen, gameMoves]);

    }

}
