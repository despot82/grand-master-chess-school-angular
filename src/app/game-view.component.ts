import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Pgn } from './pgn';
import { Game } from './models/game';
import { IosStyleTogglerComponent } from '../app/ios-style-toggler/ios-style-toggler.component';

@Component({
  selector: 'app-game',
  templateUrl: './game-view.component.html',
  styleUrls: ['./game-view.component.css']
})
export class GameViewComponent implements OnInit {

    title = 'app';
    position = 'start';
    orientation: Boolean = true;
    showNotation: Boolean = true;
    draggable: Boolean = true;
    animation: Boolean = true;

    game: Game = new Game();
    next: String;

    constructor(
      private route: ActivatedRoute,
      private router: Router,
      private pgnHelper: Pgn) {

        this.game.id = +this.route.snapshot.paramMap.get('id');
        this.game.event = this.route.snapshot.paramMap.get('event');
        this.game.fen = this.route.snapshot.paramMap.get('fen');
        this.game.moves = this.route.snapshot.paramMap.get('moves');

    }

    ngOnInit() {
        this.game.pgn2fen();
    }

    goBack() {
        this.router.navigate(['/pgn-list']);
    }


    doNextMove() {
        this.position = this.game.fenList[0];

    }

    doPreviousMove() {

    }

}
