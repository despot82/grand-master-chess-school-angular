import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChessboardModule } from './modules/chessboard/chessboard.module';

import { AppComponent } from './app.component';
import { Pgn } from './pgn';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { PgnListComponent } from './pgn-list.component';
import { GameViewComponent } from './game-view.component';
import { IosStyleTogglerComponent } from './ios-style-toggler/ios-style-toggler.component';

@NgModule({
  declarations: [
    AppComponent,
    PgnListComponent,
    GameViewComponent,
    IosStyleTogglerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChessboardModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [Pgn],
  bootstrap: [AppComponent]
})
export class AppModule { }
