import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PgnListComponent } from './pgn-list.component';
import { GameViewComponent } from './game-view.component';

const routes: Routes = [
  { path: '', component: PgnListComponent },
  { path: 'pgn-list', component: PgnListComponent },
  { path: 'game/:id/:event/:fen/:moves', component: GameViewComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
