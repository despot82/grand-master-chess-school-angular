import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class Pgn {
    pgnString: string;
    pgnParsed;

    constructor(private http: Http) {

    }

    // Preprocess each empty-line separated section, which is each either a metadata 
    // or the one liner with the game moves
    promiseToGetAllGamesFromPgn(fileName: string) {

        return new Promise((resolve, reject) => {
            this.http.get('assets/' + fileName + '.pgn').subscribe(data => {
                const games = data.text().split('\n\n');
                resolve(games);

            });

        });

    }


}
